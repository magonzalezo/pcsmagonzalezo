package com.pcs.pcschallenge.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.pcs.pcschallenge.model.Person;
import com.pcs.pcschallenge.model.RegisterHistory;

/**
 * The controller class about to basic queries.
 * 
 */
public class BasicQueryController {

	private EntityManagerFactory eMfactory;
	private EntityManager eManager;
	
	/**
	 * Prints the presence history to a specific employee.
	 * 
	 */	
	public void getPresenceHistoryEmpById(int employeeId) {
		
		@SuppressWarnings("unchecked")
		List<RegisterHistory> historyList = eManager.createQuery("select a from RegisterHistory a where id_person = "+employeeId+" order by register_date").getResultList();
		
		if (historyList !=null && !historyList.isEmpty()) {
			List<Person> personList = eManager.createQuery("select a from Person a where id_person = "+employeeId
                    , Person.class).getResultList();
			//
			System.out.println("###############################################");
			System.out.println("#####  DAILY ACTIVITY REPORT  (TimeSheet)  ####");
			System.out.println("###############################################");
			String fullName = personList.get(0).getName()+" "
					        + personList.get(0).getMiddleName()+" "
					        + personList.get(0).getLastName()+" "
					        + personList.get(0).getSecondLastName()+" ";
			System.out.println("Employee name: "+fullName);
			System.out.println("Employee ID: "+employeeId);
			System.out.println("-------------------------------------------------");
			System.out.println("- Location - Register type - Register date");
			//
			for (RegisterHistory rh : historyList) {
				System.out.println(rh.getLocation().getDescLocation()+" - "
			                      +rh.getRegisterType().getDescRegisterType()+" - "
			                      +rh.getRegisterDate());
			}
		} else {
			System.out.println("The employee do not have activity history to report.");
		}
				 
	}
	
	/**
	 * Prints the effective working hours per 
	 * day worked by a employee (by employee id).
	 * 
	 */	
	public void getEffectiveWorkTimeByEmp(int employeeId) {
		
		@SuppressWarnings("unchecked")
		List<RegisterHistory> historyList = eManager.createQuery("select a from RegisterHistory a where id_person = "+employeeId+" and id_location = 1 order by register_date").getResultList();
		
		if (historyList !=null && !historyList.isEmpty()) {
			
			List<Person> personList = eManager.createQuery("select a from Person a where id_person = "+employeeId
                    , Person.class).getResultList();
			Date today = Calendar.getInstance().getTime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
			Date in = new Date();
			Date out = new Date();
			long effectiveWorkHours = 0L;
			
			for (RegisterHistory rh : historyList) {
				
				if (rh.getRegisterType().getIdRegisterType() == 1) {
					in = rh.getRegisterDate();
				} else {
					out = rh.getRegisterDate();
					effectiveWorkHours+=(out.getTime()-in.getTime());
				}
				
			}
			
			System.out.println("%%%%%%%%%%  Productivity calculation  ("+dateFormat.format(today)+")  %%%%%%%%%%%%");
			String fullName = personList.get(0).getName()+" "
			        + personList.get(0).getMiddleName()+" "
			        + personList.get(0).getLastName()+" "
			        + personList.get(0).getSecondLastName();
			
			System.out.println("The employee ("+employeeId+")"
					+ " "+fullName+" had a productivity of: "
					+(effectiveWorkHours/3600000)+" hours today.");
			
		} else {
			System.out.println("The employee do not have activity history to calc productivity.");
		}
	}
	
	public BasicQueryController() {
		eMfactory= Persistence.createEntityManagerFactory("pcs");
		eManager = eMfactory.createEntityManager();
	}

}
