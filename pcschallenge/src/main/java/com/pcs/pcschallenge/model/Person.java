package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the person database table.
 * 
 */
@Entity
@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p")
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_person")
	private int idPerson;

	private String address;

	@Column(name="doc_number")
	private int docNumber;

	private String email;

	@Lob
	private byte[] fingerprint;

	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	@Column(name="mobile_phone")
	private int mobilePhone;

	private String name;

	@Column(name="second_last_name")
	private String secondLastName;

	//bi-directional many-to-one association to DocType
	@ManyToOne
	@JoinColumn(name="id_doc_type")
	private DocType docType;

	//bi-directional many-to-one association to PersonJobPosition
	@OneToMany(mappedBy="person")
	private List<PersonJobPosition> personJobPositions;

	//bi-directional many-to-one association to RegisterHistory
	@OneToMany(mappedBy="person")
	private List<RegisterHistory> registerHistories;

	public Person() {
	}

	public int getIdPerson() {
		return this.idPerson;
	}

	public void setIdPerson(int idPerson) {
		this.idPerson = idPerson;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getDocNumber() {
		return this.docNumber;
	}

	public void setDocNumber(int docNumber) {
		this.docNumber = docNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte[] getFingerprint() {
		return this.fingerprint;
	}

	public void setFingerprint(byte[] fingerprint) {
		this.fingerprint = fingerprint;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public int getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(int mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondLastName() {
		return this.secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public DocType getDocType() {
		return this.docType;
	}

	public void setDocType(DocType docType) {
		this.docType = docType;
	}

	public List<PersonJobPosition> getPersonJobPositions() {
		return this.personJobPositions;
	}

	public void setPersonJobPositions(List<PersonJobPosition> personJobPositions) {
		this.personJobPositions = personJobPositions;
	}

	public PersonJobPosition addPersonJobPosition(PersonJobPosition personJobPosition) {
		getPersonJobPositions().add(personJobPosition);
		personJobPosition.setPerson(this);

		return personJobPosition;
	}

	public PersonJobPosition removePersonJobPosition(PersonJobPosition personJobPosition) {
		getPersonJobPositions().remove(personJobPosition);
		personJobPosition.setPerson(null);

		return personJobPosition;
	}

	public List<RegisterHistory> getRegisterHistories() {
		return this.registerHistories;
	}

	public void setRegisterHistories(List<RegisterHistory> registerHistories) {
		this.registerHistories = registerHistories;
	}

	public RegisterHistory addRegisterHistory(RegisterHistory registerHistory) {
		getRegisterHistories().add(registerHistory);
		registerHistory.setPerson(this);

		return registerHistory;
	}

	public RegisterHistory removeRegisterHistory(RegisterHistory registerHistory) {
		getRegisterHistories().remove(registerHistory);
		registerHistory.setPerson(null);

		return registerHistory;
	}

}