package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the doc_type database table.
 * 
 */
@Entity
@Table(name="doc_type")
@NamedQuery(name="DocType.findAll", query="SELECT d FROM DocType d")
public class DocType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_doc_type")
	private int idDocType;

	@Column(name="desc_doc_type")
	private String descDocType;

	//bi-directional many-to-one association to Person
	@OneToMany(mappedBy="docType")
	private List<Person> persons;

	public DocType() {
	}

	public int getIdDocType() {
		return this.idDocType;
	}

	public void setIdDocType(int idDocType) {
		this.idDocType = idDocType;
	}

	public String getDescDocType() {
		return this.descDocType;
	}

	public void setDescDocType(String descDocType) {
		this.descDocType = descDocType;
	}

	public List<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public Person addPerson(Person person) {
		getPersons().add(person);
		person.setDocType(this);

		return person;
	}

	public Person removePerson(Person person) {
		getPersons().remove(person);
		person.setDocType(null);

		return person;
	}

}