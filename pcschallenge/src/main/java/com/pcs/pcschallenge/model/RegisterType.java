package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;


/**
* The persistent class for the register_type database table.
* 
*/
@Entity
@Table(name="register_type")
@NamedQuery(name="RegisterType.findAll", query="SELECT r FROM RegisterType r")
public class RegisterType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_register_type")
	private int idRegisterType;

	@Column(name="desc_register_type")
	private String descRegisterType;

	public RegisterType() {
	}

	public int getIdRegisterType() {
		return this.idRegisterType;
	}

	public void setIdRegisterType(int idRegisterType) {
		this.idRegisterType = idRegisterType;
	}

	public String getDescRegisterType() {
		return this.descRegisterType;
	}

	public void setDescRegisterType(String descRegisterType) {
		this.descRegisterType = descRegisterType;
	}

}