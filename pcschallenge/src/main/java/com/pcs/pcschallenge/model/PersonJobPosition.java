package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the person_job_position database table.
 * 
 */
@Entity
@Table(name="person_job_position")
@NamedQuery(name="PersonJobPosition.findAll", query="SELECT p FROM PersonJobPosition p")
public class PersonJobPosition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_person_job_position")
	private int idPersonJobPosition;

	//bi-directional many-to-one association to JobPosition
	@ManyToOne
	@JoinColumn(name="id_job_position")
	private JobPosition jobPosition;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="id_person")
	private Person person;

	public PersonJobPosition() {
	}

	public int getIdPersonJobPosition() {
		return this.idPersonJobPosition;
	}

	public void setIdPersonJobPosition(int idPersonJobPosition) {
		this.idPersonJobPosition = idPersonJobPosition;
	}

	public JobPosition getJobPosition() {
		return this.jobPosition;
	}

	public void setJobPosition(JobPosition jobPosition) {
		this.jobPosition = jobPosition;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}