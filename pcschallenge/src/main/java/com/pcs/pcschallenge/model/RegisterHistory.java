package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the register_history database table.
 * 
 */
@Entity
@Table(name="register_history")
@NamedQuery(name="RegisterHistory.findAll", query="SELECT r FROM RegisterHistory r")
public class RegisterHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_register_history")
	private int idRegisterHistory;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="register_date")
	private Date registerDate;

	//bi-directional many-to-one association to Location
	@ManyToOne
	@JoinColumn(name="id_location")
	private Location location;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="id_person")
	private Person person;

	//bi-directional many-to-one association to RegisterType
	@ManyToOne
	@JoinColumn(name="id_register_type")
	private RegisterType registerType;

	public RegisterHistory() {
	}

	public int getIdRegisterHistory() {
		return this.idRegisterHistory;
	}

	public void setIdRegisterHistory(int idRegisterHistory) {
		this.idRegisterHistory = idRegisterHistory;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public RegisterType getRegisterType() {
		return this.registerType;
	}

	public void setRegisterType(RegisterType registerType) {
		this.registerType = registerType;
	}

}