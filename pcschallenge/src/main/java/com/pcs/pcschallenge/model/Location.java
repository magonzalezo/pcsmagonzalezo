package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_location")
	private int idLocation;

	@Column(name="desc_location")
	private String descLocation;

	//bi-directional many-to-one association to RegisterHistory
	@OneToMany(mappedBy="location")
	private List<RegisterHistory> registerHistories;

	public Location() {
	}

	public int getIdLocation() {
		return this.idLocation;
	}

	public void setIdLocation(int idLocation) {
		this.idLocation = idLocation;
	}

	public String getDescLocation() {
		return this.descLocation;
	}

	public void setDescLocation(String descLocation) {
		this.descLocation = descLocation;
	}

	public List<RegisterHistory> getRegisterHistories() {
		return this.registerHistories;
	}

	public void setRegisterHistories(List<RegisterHistory> registerHistories) {
		this.registerHistories = registerHistories;
	}

	public RegisterHistory addRegisterHistory(RegisterHistory registerHistory) {
		getRegisterHistories().add(registerHistory);
		registerHistory.setLocation(this);

		return registerHistory;
	}

	public RegisterHistory removeRegisterHistory(RegisterHistory registerHistory) {
		getRegisterHistories().remove(registerHistory);
		registerHistory.setLocation(null);

		return registerHistory;
	}

}