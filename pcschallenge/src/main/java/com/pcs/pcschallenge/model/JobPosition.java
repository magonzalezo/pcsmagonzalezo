package com.pcs.pcschallenge.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the job_position database table.
 * 
 */
@Entity
@Table(name="job_position")
@NamedQuery(name="JobPosition.findAll", query="SELECT j FROM JobPosition j")
public class JobPosition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_job_position")
	private int idJobPosition;

	@Column(name="desc_job_position")
	private String descJobPosition;

	//bi-directional many-to-one association to PersonJobPosition
	@OneToMany(mappedBy="jobPosition")
	private List<PersonJobPosition> personJobPositions;

	public JobPosition() {
	}

	public int getIdJobPosition() {
		return this.idJobPosition;
	}

	public void setIdJobPosition(int idJobPosition) {
		this.idJobPosition = idJobPosition;
	}

	public String getDescJobPosition() {
		return this.descJobPosition;
	}

	public void setDescJobPosition(String descJobPosition) {
		this.descJobPosition = descJobPosition;
	}

	public List<PersonJobPosition> getPersonJobPositions() {
		return this.personJobPositions;
	}

	public void setPersonJobPositions(List<PersonJobPosition> personJobPositions) {
		this.personJobPositions = personJobPositions;
	}

	public PersonJobPosition addPersonJobPosition(PersonJobPosition personJobPosition) {
		getPersonJobPositions().add(personJobPosition);
		personJobPosition.setJobPosition(this);

		return personJobPosition;
	}

	public PersonJobPosition removePersonJobPosition(PersonJobPosition personJobPosition) {
		getPersonJobPositions().remove(personJobPosition);
		personJobPosition.setJobPosition(null);

		return personJobPosition;
	}

}