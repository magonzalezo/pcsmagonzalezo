package com.pcs.pcschallenge;

import com.pcs.pcschallenge.controller.BasicQueryController;

/**
 * Presence Control Syste
 * Author: Miguel Angel Gonzalez Ordoñez
 * Scope: backend 
 */
public class App 
{
    public static void main( String[] args )
    {
        BasicQueryController bqc = new BasicQueryController();
        //bqc.getPresenceHistoryEmpById(1);
        bqc.getEffectiveWorkTimeByEmp(1);
    }
}
