**Guia de apertura del proyecto**

Realizado por Miguel Gonzalez, para el reto de PCS (Presence Control System)

Desarrollado en eclipse y compilado en java 1.8
---

## Instalacion de modelo

Base de datos

1. Ubicar el archivo SQL de la base de datos, el cual esta en este misma base de directorio.
2. Por medio de phpmyadmin u otra herramienta realizar la importacion de la base de datos o ejecutar los bloques sql internos.

---

## Aplicativo

La carpeta contenida a este mismo nivel de directorio tiene el proyecto eclipse como tal.

1. Crear un workspace en su eclipse.
2. En la perspectiva de proyecto importar este proyecto
3. En el archivo de persistencia cambiar los parametros de conexion a su motor de base de datos.
4. Ejecutar como java application la clase App.java y ejecutar el metodo a probar.